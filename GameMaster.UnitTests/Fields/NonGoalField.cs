﻿using System;
using Xunit;
using NSubstitute;
using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;

namespace GameMaster.UnitTests.Fields
{
    public class NonGoalFieldTests : FieldTests
    {
        private readonly NormalPiece NormalPiece;
        private readonly ShamPiece ShamPiece;
        private readonly IDistanceCalculator DistanceCalculator;
        private readonly IPieceService PieceService;
        public NonGoalFieldTests()
        {
            IScore Score = Substitute.For<IScore>();
            DistanceCalculator = Substitute.For<IDistanceCalculator>();
            Field = new NonGoalField(new Point(0,0), DistanceCalculator);
            PieceService = Substitute.For<IPieceService>();
            NormalPiece = Substitute.For<NormalPiece>(PieceService);
            ShamPiece = Substitute.For<ShamPiece>(PieceService);

        }
        [Fact]
        public void PutPiece_NullShamPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((ShamPiece)null!));
        }

        [Fact]
        public void PutPiece_NullNormalPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((NormalPiece)null!));
        }

        [Fact]
        public void PutPiece_NormalPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.NonGoalField, Field.PutPiece(NormalPiece));
            Assert.Single((Field as NonGoalField)?.Pieces);
        }

        [Fact]
        public void PutPiece_ShamPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.ShamPutOnGoalArea, Field.PutPiece(ShamPiece));
            Assert.Single((Field as NonGoalField)?.Pieces);
        }

        [Fact]
        public void PutPiece_CorrectNormalPiecePut_DesactivatesPiece()
        {
            Field.PutPiece(NormalPiece);
            PieceService.Received().DeactivatePiece();
        }

        [Fact]
        public void PutPiece_CorrectShamPiecePut_DesactivatesPiece()
        {
            Field.PutPiece(ShamPiece);
            PieceService.Received().DeactivatePiece();
        }

        [Fact]
        public void PickUpPiece_EmptyField_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAny<InvalidOperationException>(() => Field.PickUpPiece());
        }

        [Fact]
        public void PickUpPiece_NotEmptyField_ThrowsInvalidOperationException()
        {
            Field.PutPiece(NormalPiece);
            Assert.ThrowsAny<InvalidOperationException>(() => Field.PickUpPiece());
        }
    }
}
