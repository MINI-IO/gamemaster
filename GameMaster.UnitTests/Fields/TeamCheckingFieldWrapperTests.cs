﻿using System;
using Xunit;
using NSubstitute;
using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;

namespace GameMaster.UnitTests.Fields
{
    public class TeamCheckingFieldWrapperTests
    {
        private IField wrappedField;
        private IField Field;
        private NormalPiece NormalPiece;
        private ShamPiece ShamPiece;
        private readonly Team team = Team.Red;
        public TeamCheckingFieldWrapperTests()
        {
            wrappedField = Substitute.For<IField>();
            Field = new TeamCheckingFieldWrapper(team, wrappedField);
            var pieceService = Substitute.For<IPieceService>();
            NormalPiece = Substitute.For<NormalPiece>(pieceService);
            ShamPiece = Substitute.For<ShamPiece>(pieceService);
        }
        [Fact]
        public void PutPiece_ShamPiece_InvokesWrappedField()
        {
            Field.PutPiece(ShamPiece);
            wrappedField.Received().PutPiece(Arg.Any<ShamPiece>());
        }

        [Fact]
        public void PutPiece_NormalPiece_InvokesWrappedField()
        {
            Field.PutPiece(NormalPiece);
            wrappedField.Received().PutPiece(Arg.Any<NormalPiece>());
        }

        [Fact]
        public void MoveHere_SameTeamAsPlayer_Moves()
        {
            var redPlayer = Substitute.For<IMover>();
            redPlayer.Team.Returns(Team.Red);
            IField wprapper = new TeamCheckingFieldWrapper(Team.Red, wrappedField);
            wprapper.MoveHere(redPlayer);
            wrappedField.Received().MoveHere(redPlayer);
        }
        [Fact]
        public void MoveHere_DifferentTeamAsPlayer_DoesntMove()
        {
            var redPlayer = Substitute.For<IMover>();
            redPlayer.Team.Returns(Team.Red);
            IField wprapper = new TeamCheckingFieldWrapper(Team.Blue, wrappedField);
            Assert.ThrowsAny<InvalidOperationException>(() => wprapper.MoveHere(redPlayer));
            wrappedField.DidNotReceiveWithAnyArgs().MoveHere(Arg.Any<IMover>());
        }
    }
}
