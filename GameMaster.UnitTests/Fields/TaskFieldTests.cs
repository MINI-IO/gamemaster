using System;
using Xunit;
using NSubstitute;
using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;

namespace GameMaster.UnitTests.Fields
{
    public class TaskFieldTests : FieldTests
    {
        private readonly NormalPiece NormalPiece;
        private readonly ShamPiece ShamPiece;
        private readonly IDistanceCalculator DistanceCalculator;

        public TaskFieldTests()
        {
            DistanceCalculator = Substitute.For<IDistanceCalculator>();
            Field = new TaskField(new Point(0,0), DistanceCalculator);
            var pieceService = Substitute.For<IPieceService>();
            NormalPiece = Substitute.For<NormalPiece>(pieceService);
            ShamPiece = Substitute.For<ShamPiece>(pieceService);
        }
        [Fact]
        public void PutPiece_NullShamPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((ShamPiece)null!));
        }

        [Fact]
        public void PutPiece_NullNormalPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((NormalPiece)null!));
        }

        [Fact]
        public void PutPiece_NormalPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.TaskField, Field.PutPiece(NormalPiece));
            Assert.Single((Field as TaskField)?.Pieces);
        }

        [Fact]
        public void PutPiece_ShamPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.TaskField, Field.PutPiece(ShamPiece));
            Assert.Single((Field as TaskField)?.Pieces);
        }
        [Fact]
        public void PutPiece_CorrectNormalPiecePut_DoesntDesactivatePiece()
        {
            Field.PutPiece(NormalPiece);
            NormalPiece.DidNotReceive().Deactivate();
        }

        [Fact]
        public void PutPiece_CorrectShamPiecePut_DoesntDesactivatePiece()
        {
            Field.PutPiece(ShamPiece);
            ShamPiece.DidNotReceive().Deactivate();
        }
        [Fact]
        public void PickUpPiece_EmptyField_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAny<InvalidOperationException>(() => Field.PickUpPiece());
        }

        [Fact]
        public void PickUpPiece_NotEmptyField_PickUpsPiece()
        {
            Field.PutPiece(ShamPiece);
            Assert.Equal(ShamPiece, Field.PickUpPiece());
        }

        [Fact]
        public void MoveHere_PieceInField_AgentPicksUp()
        {
            Field.PutPiece(NormalPiece);
            Player.Holding!.Returns<IPiece>((IPiece)null!);
            Field.MoveHere(Player);
            Player.Received().PickUpPiece();
        }
    }
}
