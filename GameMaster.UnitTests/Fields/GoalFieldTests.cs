using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;
using NSubstitute;
using System;
using Xunit;

namespace GameMaster.UnitTests.Fields
{
    public class GoalFieldTests : FieldTests
    {
        private readonly IScore Score;
        private readonly NormalPiece NormalPiece;
        private readonly ShamPiece ShamPiece;
        private readonly IDistanceCalculator DistanceCalculator;
        private readonly IPieceService PieceService;
        public GoalFieldTests()
        {
            Score = Substitute.For<IScore>();
            DistanceCalculator = Substitute.For<IDistanceCalculator>();
            Field = new GoalField(new Point(0, 0), Score,DistanceCalculator);
            PieceService = Substitute.For<IPieceService>();
            NormalPiece = Substitute.For<NormalPiece>(PieceService);
            ShamPiece = Substitute.For<ShamPiece>(PieceService);
        }
        [Fact]
        public void PutPiece_NullShamPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((ShamPiece)null!));
            Score.DidNotReceive().IncrementPoints();
        }

        [Fact]
        public void PutPiece_NullNormalPiecePut_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Field.PutPiece((NormalPiece)null!));
            Score.DidNotReceive().IncrementPoints();
        }

        [Fact]
        public void PutPiece_CorrectNormalPiecePut_IncrementsScore()
        {
            Assert.Equal(PutInfo.GoalField, Field.PutPiece(NormalPiece));
            Score.Received().IncrementPoints();
        }
        [Fact]
        public void PutPiece_TwoCorrectNormalPiecePut_IncrementsScoreOnlyOnce()
        { 
            var NormalPiece2 = Substitute.For<NormalPiece>(PieceService);
            Assert.Equal(PutInfo.GoalField, Field.PutPiece(NormalPiece));
            Assert.Equal(PutInfo.GoalField, Field.PutPiece(NormalPiece2));
            Score.Received(1).IncrementPoints();
        }

        [Fact]
        public void PutPiece_CorrectShamPiecePut_DoesntIncrementScore()
        {
            Assert.Equal(PutInfo.ShamPutOnGoalArea, Field.PutPiece(ShamPiece));
            Score.DidNotReceive().IncrementPoints();
        }

        [Fact]
        public void PutPiece_CorrectNormalPiecePut_DesactivatesPiece()
        {
            Field.PutPiece(NormalPiece);
            PieceService.Received().DeactivatePiece();
        }

        [Fact]
        public void PutPiece_CorrectShamPiecePut_DesactivatesPiece()
        {
            Field.PutPiece(ShamPiece);
            PieceService.Received().DeactivatePiece();
        }

        [Fact]
        public void PutPiece_NormalPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.GoalField, Field.PutPiece(NormalPiece));
            Assert.Single((Field as GoalField)?.Pieces);
        }

        [Fact]
        public void PutPiece_ShamPiecePut_PutsOnStack()
        {
            Assert.Equal(PutInfo.ShamPutOnGoalArea, Field.PutPiece(ShamPiece));
            Assert.Single((Field as GoalField)?.Pieces);
        }

        [Fact]
        public void PickUpPiece_EmptyField_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAny<InvalidOperationException>(() => Field.PickUpPiece());
        }

        [Fact]
        public void PickUpPiece_NotEmptyField_ThrowsInvalidOperationException()
        {
            Assert.ThrowsAny<InvalidOperationException>(() => Field.PickUpPiece());
        }
    }
}
