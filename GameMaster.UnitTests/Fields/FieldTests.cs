﻿using System;
using Xunit;
using NSubstitute;
using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;

namespace GameMaster.UnitTests.Fields
{
    public abstract class FieldTests
    {
        protected IField Field;
        protected IMover Player;

        public FieldTests()
        {
            Player = Substitute.For<IMover>();
            Player.Position.Returns(Substitute.For<IField>());
            Field = null!;
        }
        [Fact]
        public void MoveHere_NobodysInField_ChangesAgentPosition()
        {
            Field.MoveHere(Player);
            Player.Received().Position = Field;
        }

        [Fact]
        public void MoveHere_NobodysInField_ChangesFieldsAgent()
        {
            Field.MoveHere(Player);
            Assert.Equal(Player, Field.WhosHere);
        }

        [Fact]
        public void MoveHere_NobodyInField_ChengesLeftFieldAgentToNull()
        {
            var fieldPrev = Substitute.For<IField>();
            Player.Position.Returns(fieldPrev);
            Field.MoveHere(Player);
            Assert.Null(fieldPrev.WhosHere);
        }

        [Fact]
        public void MoveHere_NobodysInField_ChangesPlayerInField()
        {
            Field.MoveHere(Player);
            Assert.Equal(Player, Field.WhosHere);
        }

        [Fact]
        public void MoveHere_SomebodysInField_ThrowsException()
        {
            Field.MoveHere(Player);
            var secondPlayer = Substitute.For<IMover>();
            Assert.ThrowsAny<InvalidOperationException>(() => Field.MoveHere(secondPlayer));
        }
    }
}
