﻿using GameMaster.Model.Fields;
using NSubstitute;
using Xunit;
using System.Collections.Generic;
using System.Linq;

namespace GameMaster.UnitTests.Fields
{
    public class BasicDistanceCalculatorTests
    {
        private const int x = 107;
        private const int y = 322;

        private readonly Dictionary<Point, IField> Map;
        private readonly BasicDistanceCalculator Calculator;
        public BasicDistanceCalculatorTests()
        {
            Map = new Dictionary<Point, IField>();
            foreach(var p in from xx in Enumerable.Range(0,x)
                             from yy in Enumerable.Range(0,y)
                             select new Point(xx,yy))
            {
                IField field = Substitute.For<IField>();
                field.ContainsPickUpablePiece.Returns(false);
                field.Id.Returns(p);
                Map.Add(p, field);
            }

            Calculator = new BasicDistanceCalculator(Map);
        }
        [Fact]
        public void CalculateDistance_NoPickUpablePiecesOnMap_ReturnsNull()
        {
            Assert.Null(Calculator.CalculateDistanceToClosestPiece(new Point(8, 2)));
        }

        [Fact]
        public void CalculateDistance_OnePickUpablePiece_ReturnsDistance()
        {
            const int x = 14;
            const int y = 28;
            Map[new Point(x, y)].ContainsPickUpablePiece.Returns(true);
            int? result = Calculator.CalculateDistanceToClosestPiece(new Point(10, 73));
            Assert.Equal(49, result);
        }

        [Fact]
        public void CalculateDistance_ManyPickUpablePiece_ReturnsDistance()
        {
            var points = new List<Point>()
            {
                new Point(14, 28),
                new Point(17, 28),
                new Point(14, 23),
                new Point(17, 23),
            };
            foreach(Point p in points)
            {
                Map[p].ContainsPickUpablePiece.Returns(true);
            }
            int? result = Calculator.CalculateDistanceToClosestPiece(new Point(10, 73));
            Assert.Equal(49, result);
        }
    }
}
