﻿using GameMaster.Model.Fields;
using GameMaster.Model;
using GameMaster.Model.Pieces;
using NSubstitute;
using Xunit;
using System;
using System.Linq;

namespace GameMaster.UnitTests
{
    public class AgentTests
    {
        private readonly IField Field;
        private readonly IFieldProvider Repository;
        private readonly IPenaltyService Watch;
        private readonly IPenaltyTimeProvider PenaltyTimeProvider;
        private Agent Agent;
        public AgentTests()
        {
            Field = Substitute.For<IField>();
            Repository = Substitute.For<IFieldProvider>();
            Watch = Substitute.For<IPenaltyService>();
            PenaltyTimeProvider = Substitute.For<IPenaltyTimeProvider>();
            Agent = AgentFactory.CreateAgent(
                Repository,
                Watch,
                PenaltyTimeProvider,
                1,
                Team.Red,
                Field);
        }

        [Fact]
        public void Move_CorrectMove_SearchesAndMoves()
        {
            Field.Id.Returns(new Point(0, 0));
            var secondField = Substitute.For<IField>();
            Repository.GetFieldById(new Point(1, 0)).Returns(secondField);
            Agent.Move(Direction.positiveX);
            Repository.Received().GetFieldById(new Point(1, 0));
            secondField.Received().MoveHere(Agent);
        }

        [Fact]
        public void DestroyHeldPiece_NotHolding_ThrowsException()
        {
            Assert.ThrowsAny<InvalidOperationException>(() => Agent.DestroyHeldPiece());
        }

        [Fact]
        public void DestroyHeldPiece_Holding_DeactivatesPieceAndNotHolding()
        {
            var Held = Substitute.For<IPiece>();

            Agent = AgentFactory.CreateAgent(
                Repository,
                Watch,
                PenaltyTimeProvider,
                0,
                Team.Red,
                Field,
                Held);

            Agent.DestroyHeldPiece();
            Assert.Null(Agent.Holding);
            Held.Received(1).Deactivate();
        }

        [Fact]
        public void DiscoverDistances___ReturnsCorrectMatrix()
        {
            Field.Id.Returns(new Point(1, 1));
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                {
                    var field = Substitute.For<IField>();
                    field.CalculateDistanceToPiece().Returns(i + 10 * j);
                    Repository.GetFieldById(new Point(i, j)).Returns(field);
                    Repository.ContainsField(new Point(i, j)).Returns(true);
                }
            var result = Agent.DiscoverDistances();

            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                {
                    Assert.Equal(i + 10 * (2 - j), result[i, j]);
                }
        }

        [Fact]
        public void DiscoverDistances_AllNonExistingFields_ReturnsMatrixOfNulls()
        {
            Field.Id.Returns(new Point(1, 1));
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                {
                    Repository.ContainsField(new Point(i, j)).Returns(false);
                }

            var result = Agent.DiscoverDistances();
            foreach(var elem in result)
            {
                Assert.Null(elem);
            }
        }
    }
}
