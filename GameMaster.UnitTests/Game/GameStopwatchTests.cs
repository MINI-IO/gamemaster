﻿using GameMaster.Model.Game;
using System;
using System.Threading;
using Xunit;

namespace GameMaster.UnitTests.Game
{
    public class GameStopwatchTests
    {
        private readonly GameStopwatch Stopwatch;
        public GameStopwatchTests()
        {
            Stopwatch = new GameStopwatch();
        }

        [Fact]
        public void PauseResume_PenaltyServiceCreated_StopsPenltyServicesClock()
        {
            var PenaltyService = Stopwatch.CreatePenaltyService();
            Stopwatch.Pause();
            PenaltyService.RegisterPenalty(TimeSpan.FromMilliseconds(5));
            Thread.Sleep(10);
            Assert.True(PenaltyService.IsBlocked);
            Stopwatch.Resume();
            Thread.Sleep(10);
            Assert.False(PenaltyService.IsBlocked);
        }
    }
}
