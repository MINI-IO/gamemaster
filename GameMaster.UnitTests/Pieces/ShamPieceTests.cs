﻿using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;
using NSubstitute;
using Xunit;

namespace GameMaster.UnitTests.Pieces
{
    public class ShamPieceTests : PieceTests
    {
        public ShamPieceTests()
        {
            Piece = new ShamPiece(PieceService);
        }

        [Fact]
        public void Put_OnGoalField_ReturnsShamInfo()
        {
            IField goalField = Substitute.For<IField>();
            goalField.PutPiece(Arg.Any<ShamPiece>()).Returns(PutInfo.ShamPutOnGoalArea);
            goalField.PutPiece(Arg.Any<NormalPiece>()).Returns(PutInfo.GoalField);
            Assert.Equal(PutInfo.ShamPutOnGoalArea, Piece.PutOn(goalField));
        }
    }
}
