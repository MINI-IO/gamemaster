﻿using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;
using NSubstitute;
using System;
using Xunit;

namespace GameMaster.UnitTests.Pieces
{
    public abstract class PieceTests
    {
        protected Piece Piece;
        protected IPieceService PieceService;

        public PieceTests()
        {
            PieceService = Substitute.For<IPieceService>();
            Piece = null!;
        }

        [Fact]
        public void Desactivate_NeverDesactivatedBefore_InvokesDesactivation()
        {
            Piece.Deactivate();
            PieceService.Received(1).DeactivatePiece();
        }

        [Fact]
        public void Desactivate_DesactivatedBefore_ThrowsException()
        {
            Piece.Deactivate();
            Assert.ThrowsAny<InvalidOperationException>(() => Piece.Deactivate());
            PieceService.Received(1).DeactivatePiece();
        }
    }
}
