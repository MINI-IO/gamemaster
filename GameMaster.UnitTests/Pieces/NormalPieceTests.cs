﻿using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;
using NSubstitute;
using System;
using Xunit;

namespace GameMaster.UnitTests.Pieces
{
    public class NormalPieceTests : PieceTests
    {
        public NormalPieceTests()
        {
            Piece = new NormalPiece(PieceService);
        }

        [Fact]
        public void Put_OnGoalField_ReturnsGoalField()
        {
            IField goalField = Substitute.For<IField>();
            goalField.PutPiece(Arg.Any<ShamPiece>()).Returns(PutInfo.ShamPutOnGoalArea);
            goalField.PutPiece(Arg.Any<NormalPiece>()).Returns(PutInfo.GoalField);
            Assert.Equal(PutInfo.GoalField, Piece.PutOn(goalField));
        }
    }
}
