﻿using GameMaster.Model;
using GameMaster.Model.Game;
using System;

namespace GameMaster.DataAccess
{
    public interface ISingleGameRepository
    {
        public GameInstance Single();
        public void Register(GameInstance game);
    }

    public class SingleGameRepository : ISingleGameRepository
    {
        private GameInstance Game = null!;
        public void Register(GameInstance player)
        {
            if (Game is null)
            {
                Game = player;
            }
            else
            {
                throw new InvalidOperationException(
                    "Game already registered");
            }
        }

        public GameInstance Single()
            => Game ?? throw new InvalidOperationException(
                $"Game not registered");
    }
}
