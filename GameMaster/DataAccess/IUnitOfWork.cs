﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.DataAccess
{
    public interface IUnitOfWork
    {
        public ISingleGameRepository GameRepository { get; }
    }
}
