﻿namespace GameMaster.Model
{
    public enum PutInfo
    {
        TaskField,
        GoalField,
        NonGoalField,
        ShamPutOnGoalArea
    }
}
