﻿using System.Collections.Generic;

namespace GameMaster.Model.Fields
{
    public interface IDistanceCalculator
    {
        public int? CalculateDistanceToClosestPiece(Point Id);
    }
}
