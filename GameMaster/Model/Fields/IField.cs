﻿using System;
using System.Collections.Generic;
using System.Text;
using GameMaster.Model.Pieces;

namespace GameMaster.Model.Fields
{
    public interface IField
    {
        public Point Id { get; }
        public IMover? WhosHere { get; set; }
        public bool ContainsPickUpablePiece { get; }

        public Piece PickUpPiece();
        public PutInfo PutPiece(ShamPiece piece);
        public PutInfo PutPiece(NormalPiece piece);
        public void MoveHere(IMover player);
        public abstract DisplayableCell CreateDisplayableCell();
        public void SpawnPlayer(IMover player);
        public int? CalculateDistanceToPiece();
    }
}
