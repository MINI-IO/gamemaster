﻿using GameMaster.Model.Pieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Fields
{
    public class GoalField : Field
    {
        private IScore Score { get; }

        public override Point Id { get; }
        public override bool ContainsPickUpablePiece => false;

        public GoalField(
            Point id,
            IScore score,
            IDistanceCalculator distanceCalculator) : base(distanceCalculator)
        {
            this.Score = score;
            this.Id = id;
        }
        public override DisplayableCell CreateDisplayableCell()
        {
            if (WhosHere is null)
            {
                return new DisplayableCell() { FieldType = Pieces.Count > 0 ? FieldType.DiscoveredGoal : FieldType.UndiscoveredGoal };
            }
            else if (WhosHere.Team == Team.Red)
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.RedPlayer : FieldType.RedPlayerWithPiece };
            }
            else
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.BluePlayer : FieldType.BluePlayerWithPiece };
            }
        }

        public override Piece PickUpPiece()
        {
            throw new InvalidOperationException(
                $"Cannot't pick up from {this.GetType().Name}");
        }

        public override PutInfo PutPiece(ShamPiece piece)
        {
            if(piece is null)
            {
                throw new ArgumentNullException();
            }
            Pieces.Push(piece);
            piece.Deactivate();
            return PutInfo.ShamPutOnGoalArea;
        }

        private bool IncrementPoints => Pieces.Count == 0;


        public override PutInfo PutPiece(NormalPiece piece)
        {
            if (piece is null)
            {
                throw new ArgumentNullException();
            }
            if (IncrementPoints) Score.IncrementPoints();
            Pieces.Push(piece);
            piece.Deactivate();
            return PutInfo.GoalField;
        }
    }
}
