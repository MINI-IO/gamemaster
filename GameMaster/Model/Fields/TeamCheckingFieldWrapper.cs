﻿using GameMaster.Model.Pieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Fields
{
    public class TeamCheckingFieldWrapper : IField
    {
        private Team Team { get; }
        private IField WrappedField { get; }

        public IMover? WhosHere
        {
            get => WrappedField.WhosHere;
            set
            {
                WrappedField.WhosHere = value;
            }
        }

        public Point Id => WrappedField.Id;

        public bool ContainsPickUpablePiece => WrappedField.ContainsPickUpablePiece;

        public TeamCheckingFieldWrapper(Team team, IField field) : base()
        {
            this.Team = team;
            this.WrappedField = field;
        }

        public void MoveHere(IMover player)
        {
            if (player.Team != Team)
            {
                throw new InvalidOperationException(
                    $"{player.Team} cannot enter {Team} field");
            }

            WrappedField.MoveHere(player);
        }

        public Piece PickUpPiece()
            => WrappedField.PickUpPiece();

        public DisplayableCell CreateDisplayableCell()
        {
            if (WrappedField.CreateDisplayableCell().FieldType == FieldType.UndiscoveredNonGoal)
                return new DisplayableCell
                {
                    FieldType = Team == Team.Red ?
                    FieldType.EmptyRedArea :
                    FieldType.EmptyBlueArea,
                };

            return WrappedField.CreateDisplayableCell();
        }

        public PutInfo PutPiece(ShamPiece piece)
            => WrappedField.PutPiece(piece);


        public PutInfo PutPiece(NormalPiece piece)
            => WrappedField.PutPiece(piece);

        public void SpawnPlayer(IMover player)
            => WrappedField.SpawnPlayer(player);

        public int? CalculateDistanceToPiece()
            => WrappedField.CalculateDistanceToPiece();
    }
}
