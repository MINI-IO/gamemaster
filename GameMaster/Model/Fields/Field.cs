﻿using GameMaster.Model.Pieces;
using System;
using System.Collections.Generic;
using System.Text;


namespace GameMaster.Model.Fields
{
    public abstract class Field : IField
    {
        abstract public Point Id { get; }
        abstract public bool ContainsPickUpablePiece { get; }
        public IMover? WhosHere { get; set; }
        public Stack<Piece> Pieces { get; } = new Stack<Piece>();
        private IDistanceCalculator DistanceCalculator;

        public abstract Piece PickUpPiece();
        public abstract PutInfo PutPiece(ShamPiece piece);
        public abstract DisplayableCell CreateDisplayableCell();
        public abstract PutInfo PutPiece(NormalPiece piece);

        public Field(IDistanceCalculator distanceCalculator)
        {
            this.DistanceCalculator = distanceCalculator;
        }

        public virtual void MoveHere(IMover player)
        {
            if (WhosHere is null)
            {
                player.Position.WhosHere = null;
                WhosHere = player;
                player.Position = this;
            }
            else
            {
                throw new InvalidOperationException(
                    "Somebody already is in this field");
            }
        }
        public void SpawnPlayer(IMover player)
        {
            if (WhosHere is null)
            {
                WhosHere = player;
            }
            else
            {
                throw new InvalidOperationException(
                    "Somebody is already in this Field");
            }
        }

        public int? CalculateDistanceToPiece()
        {
            return DistanceCalculator.CalculateDistanceToClosestPiece(Id);
        }
    }
}
