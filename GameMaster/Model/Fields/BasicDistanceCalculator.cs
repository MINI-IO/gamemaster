﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace GameMaster.Model.Fields
{
    public class BasicDistanceCalculator : IDistanceCalculator
    {
        private readonly Func<Point, Point, int> Metric;


        private readonly Dictionary<Point, IField> Map;
        public BasicDistanceCalculator(Dictionary<Point, IField> map)
        {
            this.Map = map;
            this.Metric =
                (l, r) =>
                    Math.Abs(l.X - r.X)
                    +
                    Math.Abs(l.Y - r.Y);
        }

        public BasicDistanceCalculator(Dictionary<Point, IField> map, Func<Point, Point, int> metric)
        {
            this.Map = map;
            this.Metric = metric;
        }

        public int? CalculateDistanceToClosestPiece(Point Id)
            => Map.Select(d => d.Value)
                    .Where(f => f.ContainsPickUpablePiece)
                    .Select(f => (int?)Metric(Id, f.Id))
                    .Min();
    }
}
