﻿using GameMaster.Model.Pieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Fields
{
    public class NonGoalField : Field
    {
        public override Point Id { get; }

        public override bool ContainsPickUpablePiece => false;

        public NonGoalField(
            Point id,
            IDistanceCalculator distanceCalculator) : base(distanceCalculator)
        {
            this.Id = id;
        }

        public override DisplayableCell CreateDisplayableCell()
        {
            if (WhosHere is null)
            {
                return new DisplayableCell() { FieldType = Pieces.Count > 0 ? FieldType.DiscoveredNonGoal : FieldType.UndiscoveredNonGoal };
            }
            else if (WhosHere.Team == Team.Red)
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.RedPlayer : FieldType.RedPlayerWithPiece };
            }
            else
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.BluePlayer : FieldType.BluePlayerWithPiece };
            }
        }

        public override Piece PickUpPiece()
        {
            throw new InvalidOperationException(
                $"Cannot't pick up from {this.GetType().Name}");
        }

        public override PutInfo PutPiece(ShamPiece piece)
        {
            if (piece is null)
            {
                throw new ArgumentNullException();
            }
            Pieces.Push(piece);
            piece.Deactivate();
            return PutInfo.ShamPutOnGoalArea;
        }

        public override PutInfo PutPiece(NormalPiece piece)
        {
            if (piece is null)
            {
                throw new ArgumentNullException();
            }
            Pieces.Push(piece);
            piece.Deactivate();
            return PutInfo.NonGoalField;
        }
    }
}
