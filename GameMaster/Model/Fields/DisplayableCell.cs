﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model
{
    public enum FieldType
    {
        EmptyRedArea,
        EmptyBlueArea,
        EmptyTaskArea,
        RedPlayer,
        BluePlayer,
        RedPlayerWithPiece,
        BluePlayerWithPiece,
        Piece,
        DiscoveredGoal,
        UndiscoveredGoal,
        DiscoveredNonGoal,
        UndiscoveredNonGoal,
    }
    public struct DisplayableCell
    {
        public FieldType FieldType { get; set; }
    }
}
