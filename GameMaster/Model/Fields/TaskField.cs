﻿using GameMaster.Model.Pieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Fields
{
    public class TaskField : Field
    {
        public override Point Id { get; }

        public override bool ContainsPickUpablePiece => Pieces.Count > 0;

        public TaskField(
            Point id,
            IDistanceCalculator distanceCalculator) : base(distanceCalculator)
        {
            this.Id = id;
        }
        public override DisplayableCell CreateDisplayableCell()
        {
            if (WhosHere is null)
            {
                return new DisplayableCell { FieldType = Pieces.Count > 0 ? FieldType.Piece : FieldType.EmptyTaskArea };
            }
            else if (WhosHere.Team == Team.Red)
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.RedPlayer : FieldType.RedPlayerWithPiece };
            }
            else
            {
                return new DisplayableCell { FieldType = WhosHere.Holding is null ? FieldType.BluePlayer : FieldType.BluePlayerWithPiece };
            }
        }

        public override Piece PickUpPiece()
            => Pieces.Pop();

        public PutInfo PutPiece(Piece piece)
        {
            if(piece is null)
            {
                throw new ArgumentNullException();
            }
            Pieces.Push(piece);
            return PutInfo.TaskField;
        }

        public override PutInfo PutPiece(ShamPiece piece)
            => PutPiece((Piece)piece);

        public override PutInfo PutPiece(NormalPiece piece)
            => PutPiece((Piece)piece);
        public override void MoveHere(IMover player)
        {
            base.MoveHere(player);
            if (Pieces.Count > 0 && player.Holding is null)
            {
                player.PickUpPiece();
            }
        }
    }
}
