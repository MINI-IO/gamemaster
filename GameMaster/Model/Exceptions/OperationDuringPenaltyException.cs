﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Exceptions
{
    class OperationDuringPenaltyException : InvalidOperationException
    {
        public TimeSpan PenaltyLeft;
        public OperationDuringPenaltyException(TimeSpan penaltyLeft) 
        {
            this.PenaltyLeft = penaltyLeft;
        }

        public OperationDuringPenaltyException(TimeSpan penaltyLeft, string message)
            : base(message) 
        {
            this.PenaltyLeft = penaltyLeft;
        }

        public OperationDuringPenaltyException(
            TimeSpan penaltyLeft,
            string message,
            Exception inner)
            : base(message, inner)
        {
            this.PenaltyLeft = penaltyLeft;
        }

        public override string Message
            => $"Penalty left: {PenaltyLeft.Milliseconds} ms\n" +  base.Message;
    }
}
