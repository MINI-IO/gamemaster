﻿using GameMaster.Model.Fields;

namespace GameMaster.Model
{
    public static class AgentFactory
    {
        public static Agent CreateAgent(
            IFieldProvider FieldRepository,
            IPenaltyService StopWatch,
            IPenaltyTimeProvider PenaltyTimeProvider,
            int Id,
            Team Team,
            IField Position,
            IPiece? Holding = null)
        {
            Agent player = new Agent(
                FieldRepository,
                StopWatch,
                PenaltyTimeProvider,
                Id,
                Team,
                Position,
                Holding);

            Position.SpawnPlayer(player);
            return player;
        }
    }
}
