﻿using System;
using GameMaster.Model.Fields;
using GameMaster.Model.Exceptions;
using System.Reflection;

namespace GameMaster.Model
{
    public enum Team
    {
        Red = 0,
        Blue = 1
    }

    public enum Direction
    {
        positiveY,
        negativeY,
        positiveX,
        negativeX
    }
    
    public interface IMover
    {
        public Team Team { get; }
        public IField Position { get; set; }
        public IPiece? Holding { get; }
        public void PickUpPiece();
    }

    public interface IPieceCarrier
    {
        public IPiece? Holding { get; }
        public void DestroyHeldPiece();
        public PutInfo PutPiece();
        public void PickUpPiece();

    }

    public interface IFieldProvider
    {
        public IField GetFieldById(Point Id);
        public bool ContainsField(Point Id);
    }

    public interface IPenaltyService
    {
        public void RegisterPenalty(TimeSpan time);
        public TimeSpan PenaltyTimeLeft { get; }
        public bool IsBlocked { get; }
    }

    public interface IPenaltyTimeProvider
    {
        public TimeSpan GetPenaltyTime(string actionName);
    }

    public class Agent : IMover, IPieceCarrier
    {
        public int Id { get; }
        public Team Team { get; }
        public bool IsLeader { get; }
        public IPiece? Holding { get; private set; }
        public IField Position { get; set; }
        public IPenaltyService StopWatch { get; }
        public IPenaltyTimeProvider PenaltyTimeProvider { get; }

        private int X => Position.Id.X;
        private int Y => Position.Id.Y;

        private readonly IFieldProvider FieldRepository;

        public Agent(
            IFieldProvider fieldRepository,
            IPenaltyService stopWatch,
            IPenaltyTimeProvider penaltyTimeProvider,
            int Id,
            Team Team,
            IField Position,
            IPiece? Holding = null)
        {
            this.FieldRepository = fieldRepository;
            this.StopWatch = stopWatch;
            this.PenaltyTimeProvider = penaltyTimeProvider;
            this.Id = Id;
            this.Team = Team;
            this.Position = Position;
            this.Holding = Holding;
        }

        public void Pause()
        {

        }

        public (int?,bool) Move(Direction dir)
        {
            PreAction();
            Point Id = dir switch
            {
                Direction.positiveX => new Point(X + 1, Y),
                Direction.negativeX => new Point(X - 1, Y),
                Direction.negativeY => new Point(X, Y - 1),
                Direction.positiveY => new Point(X, Y + 1),
                _ => throw new NotImplementedException(
                    $"Switch {dir}")
            };
            bool holding = Holding is null;
            FieldRepository.GetFieldById(Id).MoveHere(this);
            PostAction();
            return (Position.CalculateDistanceToPiece(),
                holding != (Holding is null));
        }

        public void DestroyHeldPiece()
        {
            PreAction();
            if (Holding is null)
            {
                throw new InvalidOperationException(
                    "Tried destroying piece when holding nothing");
            }
            Holding.Deactivate();
            Holding = null;
            PostAction();
        }

        public bool CheckHeldPieceForSham()
        {
            PreAction();
            if (Holding is null)
            {
                throw new InvalidOperationException(
                    "Tried checking piece when holding nothing");
            }
            PostAction();
            return Holding.IsSham;
        }

        public int?[,] DiscoverDistances()
        {
            PreAction();
            var result = new int?[3,3];
            for(int i = -1; i <= 1; ++i)
                for(int j = -1; j <= 1; ++j)
                {
                    Point id = new Point(X + i, Y - j);
                    result[i + 1, j + 1] = FieldRepository.ContainsField(id) ?
                        (int?)FieldRepository.GetFieldById(id).CalculateDistanceToPiece()
                        :
                        null;
                }

            PostAction();
            return result;
        }

        public PutInfo PutPiece()
        {
            PreAction();
            PostAction();
            var putInfo = Holding?.PutOn(Position) ??
                  throw new InvalidOperationException(
                      "Tried putting piece when holding nothing");
            Holding = null;
            return putInfo;
        }

        public void PickUpPiece()
        {
            PreAction();
            Holding = Position.PickUpPiece();
            PostAction();
        }

        private void PreAction()
        {
            if(StopWatch.IsBlocked)
            {
                throw new OperationDuringPenaltyException(
                    StopWatch.PenaltyTimeLeft);
            }
        }

        private void PostAction([System.Runtime.CompilerServices.CallerMemberName] string callerName = "")
        {
            var time = PenaltyTimeProvider?.GetPenaltyTime(callerName) ?? TimeSpan.FromSeconds(1);
            StopWatch.RegisterPenalty(time);
        }
    }
}
