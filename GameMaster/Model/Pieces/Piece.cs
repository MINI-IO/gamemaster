﻿using GameMaster.Model.Fields;
using System;

namespace GameMaster.Model
{
    public interface IPiece
    {
        public bool IsSham { get; }
        public PutInfo PutOn(IField field);
        public void Deactivate();
    }
    public abstract class Piece : IPiece
    {
        public abstract bool IsSham { get; }
        private readonly IPieceService PieceService;
        private bool _isActive = true;
        public Piece(IPieceService pieceService)
        {
            PieceService = pieceService;
        }
        public abstract PutInfo PutOn(IField field);
        public void Deactivate()
        {
            if (!_isActive)
            {
                throw new InvalidOperationException(
                    "Piece was already desactivated");
            }

            PieceService?.DeactivatePiece();
            _isActive = false;
        }
    }

}
