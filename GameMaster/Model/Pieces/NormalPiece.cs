﻿using GameMaster.Model.Fields;

namespace GameMaster.Model.Pieces
{
    public class NormalPiece : Piece
    {
        public override bool IsSham => false;
        public NormalPiece(IPieceService pieceService) : base(pieceService) { }

        public override PutInfo PutOn(IField field)
        {
            return field.PutPiece(this);
        }
    }
}
