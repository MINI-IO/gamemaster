﻿using GameMaster.Model.Fields;

namespace GameMaster.Model.Pieces
{
    public class ShamPiece : Piece
    {
        public override bool IsSham => true;
        public ShamPiece(IPieceService pieceService) : base(pieceService) { }
        public override PutInfo PutOn(IField field)
            => field.PutPiece(this);
    }
}
