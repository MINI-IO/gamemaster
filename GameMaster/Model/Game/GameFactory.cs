﻿using GameMaster.Model.Fields;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Game
{
    public static class GameFactory
    {
        public static GameInstance CreateGame(ConfigurationViewModel configurationViewModel, ISenderService sender)
        {
            IGameStopwatch gameStopwatch = new GameStopwatch();
            IScore red = new BasicScore();
            IScore blue = new BasicScore();
            Board board = new Board(
                new CheckersGoalPattern(
                    configurationViewModel.BoardWidth,
                    configurationViewModel.BoardHeight,
                    configurationViewModel.GoalAreaHeight,
                    configurationViewModel.NumberOfGoals),
                blue,
                red,
                configurationViewModel.BoardWidth,
                configurationViewModel.BoardHeight,
                configurationViewModel.GoalAreaHeight,
                configurationViewModel.ShamPieceProbability,
                configurationViewModel.NumberOfPieces);
            var gameInstance = new GameInstance(
                gameStopwatch,
                red,
                blue,
                board,
                sender,
                configurationViewModel);

            return gameInstance;
        }
    }
}
