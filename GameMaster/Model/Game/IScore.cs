﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model
{
    public interface IScore
    {
        public int Score { get; }
        public void  IncrementPoints();
    }

    public class BasicScore : IScore
    {
        public int Score { get; private set; } = 0;
        public BasicScore()
        {
        }

        public void IncrementPoints()
        {
            Score++;
        }
    }
}
