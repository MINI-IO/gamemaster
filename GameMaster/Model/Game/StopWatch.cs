﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Model.Game
{
    public interface IGameStopwatch
    {
        public void Pause();
        public void Resume();
        public IPenaltyService CreatePenaltyService();
        public bool IsPaused();
    }
    public class GameStopwatch : IGameStopwatch
    {
        private class StopWatchPenaltyService : IPenaltyService
        {
            private readonly Stopwatch Stopwatch;

            private TimeSpan LockedUntill = TimeSpan.MinValue;
            public TimeSpan PenaltyTimeLeft => IsBlocked ? 
                LockedUntill - Stopwatch.Elapsed
                :
                TimeSpan.Zero;

            public bool IsBlocked => Stopwatch.Elapsed < LockedUntill;

            public void RegisterPenalty(TimeSpan time)
            {
                var elapsed = Stopwatch.Elapsed;
                if(elapsed < LockedUntill)
                {
                    throw new InvalidOperationException(
                        "Penalty registration while locked");
                }

                LockedUntill = elapsed + time;
            }
            public StopWatchPenaltyService(Stopwatch stopwatch)
            {
                this.Stopwatch = stopwatch;
            }
        }

        private readonly Stopwatch Stopwatch = new Stopwatch();

        public GameStopwatch()
        {
            Stopwatch.Start();
        }

        public bool IsPaused() => !Stopwatch.IsRunning;

        public void Pause()
            => Stopwatch.Stop();

        public void Resume()
            => Stopwatch.Start();
        

        public IPenaltyService CreatePenaltyService()
        {
            return new StopWatchPenaltyService(Stopwatch);
        }
    }

}
