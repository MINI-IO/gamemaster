﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameMaster.Model.Fields;
using GameMaster.Model.Pieces;

namespace GameMaster.Model.Game
{
    public interface IGoalPatternProvider
    {
        public bool IsGoal(Point p);
    }

    public class CheckersGoalPattern : IGoalPatternProvider
    {
        private List<Point> redGoals = new List<Point>();
        private List<Point> blueGoals = new List<Point>();
        public CheckersGoalPattern(
            int width,
            int height,
            int goalAreaHeight,
            int numberOfGoals)
        {
            Random rand = new Random();
            if (numberOfGoals > width * numberOfGoals)
            {
                numberOfGoals = width * numberOfGoals;
            }
            int leftGoals = numberOfGoals;
            int x, y;
            Point p;
            while(leftGoals > 0)
            {
                x = rand.Next(width);
                y = rand.Next(height - goalAreaHeight, height);
                p = new Point(x, y);
                if (!redGoals.Contains(p))
                {
                    redGoals.Add(p);
                    leftGoals--;
                }
            }
            leftGoals = numberOfGoals;
            while (leftGoals > 0)
            {
                x = rand.Next(width);
                y = rand.Next(goalAreaHeight);
                p = new Point(x, y);
                if (!blueGoals.Contains(p))
                {
                    blueGoals.Add(p);
                    leftGoals--;
                }
            }
        }
        public bool IsGoal(Point p)
        {
            return redGoals.Contains(p) || blueGoals.Contains(p);
        }
    }

    public class Board : IFieldProvider, IPieceService
    {
        public int Width { get; }
        public int Height { get; }
        public int GoalAreaHeight { get; }

        public Dictionary<Point, IField> Map { get; }
            = new Dictionary<Point, IField>();

        public int X { get; }
        public int Y { get; }
        private double shamProbability;

        public Board(
            IGoalPatternProvider goalPattern,
            IScore blueTeamScore,
            IScore redTeamScore,
            int x,
            int y,
            int goalAreaHeight,
            double shamProbability,
            int numberOfPieces)
        {
            if(2 * goalAreaHeight >= y)
            {
                throw new ArgumentException(
                    $"{nameof(y)}:{y} should be larger than " +
                    $"2 * {nameof(goalAreaHeight)}:{goalAreaHeight}");
            }

            Width = x;
            Height = y;
            GoalAreaHeight = goalAreaHeight;

            var distanceCalculator = new BasicDistanceCalculator(Map);

            var yEnumerator = Enumerable.Range(0, y);
            var taskAreaHeight = y - 2 * goalAreaHeight;

            foreach(var point in from xx in Enumerable.Range(0, x)
                                 from yy in yEnumerator.Take(goalAreaHeight)
                                 select new Point(xx, yy))
            {
                IField field = GoalAreaFieldFactory(
                    goalPattern,
                    distanceCalculator,
                    blueTeamScore,
                    redTeamScore,
                    Team.Blue,
                    point);

                Map.Add(point, field);
            }

            foreach (var point in from xx in Enumerable.Range(0, x)
                                  from yy in yEnumerator.Skip(goalAreaHeight).Take(taskAreaHeight)
                                  select new Point(xx, yy))
            {
                IField field = new TaskField(point, distanceCalculator);

                Map.Add(point, field);
            }

            foreach (var point in from xx in Enumerable.Range(0, x)
                              from yy in yEnumerator.Skip(goalAreaHeight + taskAreaHeight).Take(goalAreaHeight)
                              select new Point(xx, yy))
            {
                IField field = GoalAreaFieldFactory(
                    goalPattern,
                    distanceCalculator,
                    blueTeamScore,
                    redTeamScore,
                    Team.Red,
                    point);

                Map.Add(point, field);
            }

            for (int i = 0; i < numberOfPieces; ++i) this.SpawnPiece();
            X = x;
            Y = y;
            GoalAreaHeight = goalAreaHeight;
            this.shamProbability = shamProbability;
        }

        private static IField GoalAreaFieldFactory(
            IGoalPatternProvider goalPattern,
            IDistanceCalculator distanceCalculator,
            IScore blueTeamScore,
            IScore redTeamScore,
            Team team,
            Point point)
        {
            var score = team switch
            {
                Team.Red => redTeamScore,
                Team.Blue => blueTeamScore,
                _ => throw new NotImplementedException(
                    $"{team} is unknown value")
            };

            IField field = goalPattern.IsGoal(point) ? (IField)
                new GoalField(point, score, distanceCalculator)
                :
                new NonGoalField(point, distanceCalculator);

            return new TeamCheckingFieldWrapper(team, field);
        }


        Random random = new Random();
        public virtual IField GetNextSpawnPosition(Team team)
        {
            IField field;
            do
            {
                int y = random.Next(GoalAreaHeight, Height - GoalAreaHeight - 1);
                int x = random.Next(0, Width);
                field = Map[new Point(x, y)];
            }
            while (!(field.WhosHere is null));

            return field;
        }

        public virtual IField GetFieldById(Point Id)
            => Map[Id];

        public virtual bool ContainsField(Point Id)
            => Map.ContainsKey(Id);

        public virtual void DeactivatePiece()
            => SpawnPiece();

        private void SpawnPiece()
        {
            int y = random.Next(GoalAreaHeight, Height - GoalAreaHeight);
            int x = random.Next(0, Width);
            double sham = random.NextDouble();
            if (sham < shamProbability)
            {
                Map[new Point(x, y)].PutPiece(new ShamPiece(this));
            }
            else
            {
                Map[new Point(x, y)].PutPiece(new NormalPiece(this));
            }            
        }
    }
}
