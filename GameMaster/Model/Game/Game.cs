﻿using GameMaster.Model.Fields;
using System.Collections.Generic;
using System;
using System.Linq;

#nullable disable warnings
namespace GameMaster.Model.Game
{
    public interface IUserControlable
    {
        public void Pause();
        public void Resume();
        public void Start();
    }

    public interface ISenderService
    {
        public void SendStartMessage();
        public void SendPauseMessage();
        public void SendResumeMessage();
        public void SendEndMessage();
    }

    public class GameInstance : IUserControlable
    {
        public Board Board { get; }
        public ConfigurationViewModel Configuration { get; }
        public bool GameOver { get; private set; }
        private readonly HashSet<(int sender, int reciver)> LegalInformationReplies
            = new HashSet<(int sender, int reciver)>();
        public IScore RedTeamScore;
        public IScore BlueTeamScore;
        private readonly IGameStopwatch Watch;
        private readonly ISenderService SenderService;

        private bool started = false;

        public Dictionary<int, Agent> Agents = new Dictionary<int, Agent>();

        public GameInstance(
            IGameStopwatch Watch,
            IScore ReadTeamScore,
            IScore BlueTeamScore,
            Board Map,
            ISenderService senderService,
            ConfigurationViewModel configuration)
        {

            this.Watch = Watch;
            this.RedTeamScore = ReadTeamScore;
            this.BlueTeamScore = BlueTeamScore;
            this.SenderService = senderService;
            this.Board = Map;
            this.Configuration = configuration;
        }

        public void Join(int id, Team team)
        {
            var blueNumber = Agents.Where(a => a.Value.Team == Team.Blue).Count();
            var redNumber = Agents.Where(a => a.Value.Team == Team.Red).Count();
            if (started)
            {
                throw new InvalidOperationException(
                    "Cannot Join after start");
            }
            if (team == Team.Blue && blueNumber >= Configuration.TeamPlayersNumber)
            {
                throw new InvalidOperationException(
                    "Too many blue players");
            }
            if (team == Team.Red && redNumber >= Configuration.TeamPlayersNumber)
            {
                throw new InvalidOperationException(
                    "Too many red players");
            }

            var agent = AgentFactory.CreateAgent(
                Board,
                Watch.CreatePenaltyService(),
                null!,
                id,
                team,
                Board.GetNextSpawnPosition(team)
                );

            Agents.Add(id, agent);
        }

        public void Pause()
        {
            if (!Watch.IsPaused())
            {
                SenderService.SendPauseMessage();
                Watch.Pause();
            }
        }

        public void Resume()
        {
            if (Watch.IsPaused())
            {
                SenderService.SendResumeMessage();
                Watch.Resume();
            }
        }

        public void End()
        {
            Watch.Pause();
            SenderService.SendEndMessage();
        }

        public void Start()
        {
            if (started)
            {
                throw new InvalidOperationException(
                    "Cannot start second time");
            }

            SenderService.SendStartMessage();
            started = true;
        }

        public void CheckIfGameOver()
        {
            if (BlueTeamScore.Score == Configuration.NumberOfGoals ||
                RedTeamScore.Score == Configuration.NumberOfGoals)
            {
                GameOver = true;
                this.End();
            }
        }
    }
}
