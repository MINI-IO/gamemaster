﻿using GameMaster.Communication.DTOs;
using GameMaster.Communication.DTOs.Send;
using GameMaster.Model;
using GameMaster.Model.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Helpers
{
    public static class SendingHelper
    {
        private static readonly Dictionary<Type, int> MessageIds = new Dictionary<Type, int>
        {
            [typeof(CheckShamDTO)] = 101,
            [typeof(DestroyPieceDTO)] = 102,
            [typeof(DiscoveryDTO)] = 103,
            [typeof(GameEndDTO)] = 104,
            [typeof(GameStartDTO)] = 105,
            [typeof(ExchangeInfoRequestDTO)] = 106,
            [typeof(JoinResponseDTO)] = 107,
            [typeof(MoveDTO)] = 108,
            [typeof(PickUpPieceDTO)] = 109,
            [typeof(PutPieceDTO)] = 110,
            [typeof(PauseDTO)] = 111,
            [typeof(ResumeDTO)] = 112,
            [typeof(WrongMoveDTO)] = 901,
            [typeof(WrongLeavePieceDTO)] = 902,
            [typeof(WrongPutPieceDTO)] = 903,
            [typeof(PenaltyErrorDTO)] = 904,
            [typeof(UndefinedErrorDTO)] = 905,
        };

        public static SendDTO WrapToSend(this ISendDTO sendDTO, int agentId)
            => new SendDTO
            {
                Payload = sendDTO,
                AgentId = agentId,
                MessageId = MessageIds[sendDTO.GetType()],
            };

        public static string PutInfoToString(this PutInfo putInfo)
            => putInfo switch
            {
                PutInfo.GoalField => "NormalOnGoalField",
                PutInfo.NonGoalField => "NormalOnNonGoalField",
                PutInfo.TaskField => "TaskField",
                PutInfo.ShamPutOnGoalArea => "SchamOnGoalArea",
                _ => throw new NotImplementedException(),
            };

        public static Func<PenaltyErrorDTO?> TryUpdateAgent(Action action)
            => () =>
            {
                try
                {
                    action();
                }
                catch (OperationDuringPenaltyException e)
                {
                    return new PenaltyErrorDTO
                    {
                        WaitUntil = DateTime.Now + e.PenaltyLeft,
                    };
                };

                return null;
            };

        public static Func<T, PenaltyErrorDTO?> TryUpdateAgent<T>(Action<T> action)
            => t =>
            {
                try
                {
                    action(t);
                    return null;
                }
                catch (OperationDuringPenaltyException e)
                {
                    return new PenaltyErrorDTO
                    {
                        WaitUntil = DateTime.Now + e.PenaltyLeft,
                    };
                };
            };

        public static Func<(PenaltyErrorDTO? Error, T Value)> TryUpdateAgent<T>(Func<T> func)
            => () =>
            {
                try
                {
                    return (null, func());
                }
                catch (OperationDuringPenaltyException e)
                {
                    return (new PenaltyErrorDTO
                    {
                        WaitUntil = DateTime.Now + e.PenaltyLeft,
                    }, default);
                };
            };

        public static Func<T, (PenaltyErrorDTO? Error, R Value)> TryUpdateAgent<T, R>(Func<T, R> func)
            => t =>
            {
                try
                {
                    return (null, func(t));
                }
                catch (OperationDuringPenaltyException e)
                {
                    return (new PenaltyErrorDTO
                    {
                        WaitUntil = DateTime.Now + e.PenaltyLeft,
                    }, default);
                };
            };
    }
}
