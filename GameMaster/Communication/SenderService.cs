﻿using GameMaster.Communication.DTOs;
using GameMaster.Communication.DTOs.Send;
using GameMaster.Communication.Helpers;
using GameMaster.DataAccess;
using GameMaster.Model;
using GameMaster.Model.Game;
using IoCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMaster.Communication
{
    public class SenderService : ISenderService
    {
        private readonly IMessageSender messageSender;
        private readonly ISingleGameRepository gameRepository;
        private GameInstance Game => gameRepository.Single();

        public SenderService(IMessageSender messageSender, ISingleGameRepository gameRepository)
        {
            this.messageSender = messageSender;
            this.gameRepository = gameRepository;
        }

        public void SendEndMessage()
        {
            var gameEnd = new GameEndDTO()
            {
                Winner = Game.BlueTeamScore.Score > Game.RedTeamScore.Score ? "blue" : "red"
            };

            foreach (var id in Game.Agents.Keys)
            {
                messageSender.SendMessage(gameEnd.WrapToSend(id));
            }
        }

        public void SendPauseMessage()
        {
            var pause = new PauseDTO();

            foreach (var id in Game.Agents.Keys)
            {
                messageSender.SendMessage(pause.WrapToSend(id));
            }
        }

        public void SendResumeMessage()
        {
            var resume = new ResumeDTO();

            foreach (var id in Game.Agents.Keys)
            {
                messageSender.SendMessage(resume.WrapToSend(id));
            }
        }

        public void SendStartMessage()
        {
            var redTeamIds = Game.Agents.Where(a => a.Value.Team == Team.Red).Select(a => a.Key);
            var blueTeamIds = Game.Agents.Where(a => a.Value.Team == Team.Blue).Select(a => a.Key);
            var redLeader = Game.Agents.Where(a => a.Value.Team == Team.Red && a.Value.IsLeader).FirstOrDefault().Key;
            var blueLeader = Game.Agents.Where(a => a.Value.Team == Team.Blue && a.Value.IsLeader).FirstOrDefault().Key;
            foreach (var agent in Game.Agents.Values)
            {
                var gameStart = new GameStartDTO()
                {
                    AgentID = agent.Id,
                    AlliesIDs = (agent.Team == Team.Blue ? blueTeamIds : redTeamIds)
                        .Where(id => id != agent.Id).ToList(),
                    LeaderID = agent.Team == Team.Blue ? blueLeader : redLeader,
                    EnemiesIDs = null!,
                    TeamID = agent.Team == Team.Blue ? "blue" : "red",
                    BoardSize = new GameStartDTO.BoardSizeDTO()
                    {
                        X = Game.Board.X,
                        Y = Game.Board.Y,
                    },
                    GoalAreaSize = Game.Board.GoalAreaHeight,
                    NumberOfPlayers = new GameStartDTO.NumberOfPlayersDTO()
                    {
                        Allies = (agent.Team == Team.Blue ? blueTeamIds : redTeamIds).Count(),
                        Enemies = (agent.Team == Team.Blue ? blueTeamIds : redTeamIds).Count(),
                    },
                    Penalties = new GameStartDTO.PenaltiesDTO()
                    {
                        Move = Game.Configuration.MovePenaltyTime.ToString(),
                        CheckForScham = Game.Configuration.CheckShamPenaltyTime.ToString(),
                        Discovery = Game.Configuration.DiscoverPenaltyTime.ToString(),
                        DestroyPiece = Game.Configuration.DestroyPiecePenaltyTime.ToString(),
                        PutPiece = Game.Configuration.PutPenaltyTime.ToString(),
                        InformationExchange = Game.Configuration.InformationExchangePenaltyTime.ToString()
                    },
                    ShamPieceProbability = Game.Configuration.ShamPieceProbability,
                    Position = new GameStartDTO.PositionDTO()
                    {
                        X = agent.Position.Id.X,  // CZY POSITION ID JEST USTAWIANE ZGODNIE Z USTALENIAMI????!!!!
                        Y = agent.Position.Id.Y
                    }
                };

                messageSender.SendMessage(gameStart.WrapToSend(agent.Id));
            }
        }
    }
}
