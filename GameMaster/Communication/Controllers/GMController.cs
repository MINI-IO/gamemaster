﻿using GameMaster.Communication.DTOs;
using GameMaster.DataAccess;
using GameMaster.Model;
using IoCommunication;
using System;
using System.Collections.Generic;
using System.Text;
using static GameMaster.Communication.Helpers.SendingHelper;
using GameMaster.Model.Game;
using System.Threading;

namespace GameMaster.Communication.Controllers
{
    public class GMController : IController
    {
        private readonly ISingleGameRepository gameRepository;
        private GameInstance Game => gameRepository.Single();

        public GMController(ISingleGameRepository gameRepository)
        {
            this.gameRepository = gameRepository;
        }

        [IoConnMethod(001)]
        public SendDTO ShamCheck(MessageWrapperDTO mw, DTOs.Receive.CheckShamDTO checkSham)
        {
            var (error, sham) = TryUpdateAgent(Game.Agents[mw.AgentId].CheckHeldPieceForSham)();

            return (error as DTOs.Send.ISendDTO ?? new DTOs.Send.CheckShamDTO
            {
                Sham = sham
            }).WrapToSend(mw.AgentId);
        }

        [IoConnMethod(002)]
        public SendDTO DestroyPiece(MessageWrapperDTO mw, DTOs.Receive.DestroyPieceDTO destroyPiece)
        {
            var error = TryUpdateAgent(Game.Agents[mw.AgentId].DestroyHeldPiece)();

            return (error as DTOs.Send.ISendDTO ?? new DTOs.Send.DestroyPieceDTO()).WrapToSend(mw.AgentId);
        }

        [IoConnMethod(003)]
        public SendDTO Discovery(MessageWrapperDTO mw, DTOs.Receive.DiscoveryDTO discovery)
        {
            throw new NotImplementedException();
        }

        [IoConnMethod(004)]
        public SendDTO ExchangeInfoResponse(MessageWrapperDTO mw, DTOs.Receive.ExchangeInfoResponseDTO exchangeInfoResponse)
        {
            // XD, nie ma w dokumentacji
            throw new NotImplementedException();
        }

        [IoConnMethod(005)]
        public SendDTO ExchangeInfoRequest(MessageWrapperDTO mw, DTOs.Receive.ExchangeInfoRequestDTO exchangeInfoRequest)
        {
            // Agent jakiś timeout powinien dostać

            return new DTOs.Send.ExchangeInfoRequestDTO
            {
                AskingID = mw.AgentId,
                Leader = Game.Agents[mw.AgentId].IsLeader,
                TeamId = Game.Agents[mw.AgentId].Team.ToString().ToLower(),
            }.WrapToSend(exchangeInfoRequest.AskedAgentID);
        }

        [IoConnMethod(006)]
        public SendDTO JoinRequest(MessageWrapperDTO mw, DTOs.Receive.JoinRequestDTO joinRequest)
        {
            var team = joinRequest.TeamId switch
            {
                "red" => Team.Red,
                "blue" => Team.Blue,
                _ => throw new InvalidOperationException(),
            };

            var joinResponse = new DTOs.Send.JoinResponseDTO
            {
                AgentId = mw.AgentId,
            };

            try
            {
                Game.Join(mw.AgentId, team);

                joinResponse.Accepted = true;
            }
            catch (InvalidOperationException)
            {
                joinResponse.Accepted = false;
            }

            return joinResponse.WrapToSend(mw.AgentId);
        }

        [IoConnMethod(007)]
        public SendDTO Move(MessageWrapperDTO mw, DTOs.Receive.MoveDTO move)
        {
            var agent = Game.Agents[mw.AgentId];

            try
            {
                var (error, distance) = TryUpdateAgent<Direction, (int?, bool)>(agent.Move)(ToDirection(move.Direction));

                return (error as DTOs.Send.ISendDTO ?? new DTOs.Send.MoveDTO
                {
                    MadeMove = true,
                    ClosestPiece = !(agent.Holding is null) ? 0 : distance.Item1,
                    PickedUpPiece = !(agent.Holding is null),
                    CurrentPosition = new DTOs.Send.MoveDTO.CurrentpositionDTO
                    {
                        X = agent.Position.Id.X,
                        Y = agent.Position.Id.Y,
                    }
                }).WrapToSend(mw.AgentId);
            }
            catch (InvalidOperationException)
            {
                return new DTOs.Send.WrongMoveDTO
                {
                    Position = new DTOs.Send.WrongMoveDTO.PositionDTO
                    {
                        X = agent.Position.Id.X,
                        Y = agent.Position.Id.Y,
                    }
                }.WrapToSend(mw.AgentId);
            }
        }

        [IoConnMethod(008)]
        public SendDTO PickUpPiece(MessageWrapperDTO mw, DTOs.Receive.PickUpPieceDTO pickUpPiece)
        {
            throw new NotImplementedException();
        }

        [IoConnMethod(009)]
        public SendDTO PutPiece(MessageWrapperDTO mw, DTOs.Receive.PutPieceDTO putPiece)
        {
            try
            {
                var (error, putInfo) = TryUpdateAgent(Game.Agents[mw.AgentId].PutPiece)();
                Game.CheckIfGameOver();

                return (error as DTOs.Send.ISendDTO ?? new DTOs.Send.PutPieceDTO
                {
                    PutPieceInfo = putInfo.PutInfoToString(),
                }).WrapToSend(mw.AgentId);
            }
            catch (InvalidOperationException)
            {
                return new DTOs.Send.WrongPutPieceDTO
                {
                    ErrorSubtype = "AgentNotHolding",
                }.WrapToSend(mw.AgentId);
            }
        }

        private Direction ToDirection(string direction)
            => direction switch
            {
                "negativeX" => Direction.negativeX,
                "positiveX" => Direction.positiveX,
                "negativeY" => Direction.negativeY,
                "positiveY" => Direction.positiveY,
                _ => throw new ArgumentOutOfRangeException(),
            };
    }
}
