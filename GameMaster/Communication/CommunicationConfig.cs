﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication
{
    public class CommunicationConfig
    {
        public string? CsIP { get; set; }
        public int CsPort { get; set; }
    }
}
