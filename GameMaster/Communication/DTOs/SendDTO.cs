﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs
{
    public class SendDTO
    {
        public int MessageId { get; set; }
        public int AgentId { get; set; }
        public object Payload { get; set; } = null!;
    }
}
