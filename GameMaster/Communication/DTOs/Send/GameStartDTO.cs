﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class GameStartDTO : ISendDTO
    {
        public int AgentID { get; set; }
        public List<int> AlliesIDs { get; set; } = new List<int>();
        public int LeaderID { get; set; }
        public List<int> EnemiesIDs { get; set; } = new List<int>();
        public string TeamID { get; set; } = string.Empty;
        public BoardSizeDTO BoardSize { get; set; } = null!;
        public int GoalAreaSize { get; set; }
        public NumberOfPlayersDTO NumberOfPlayers { get; set; } = null!;
        public int NumberOfPieces { get; set; }
        public int NumberOfGoals { get; set; }
        public PenaltiesDTO Penalties { get; set; } = null!;
        public double ShamPieceProbability { get; set; }
        public PositionDTO Position { get; set; } = null!;

        public class BoardSizeDTO
        {
            public int X { get; set; }
            public int Y { get; set; }
        }

        public class NumberOfPlayersDTO
        {
            public int Allies { get; set; }
            public int Enemies { get; set; }
        }

        public class PenaltiesDTO
        {

            public string Move { get; set; } = string.Empty;
            public string CheckForScham { get; set; } = string.Empty;
            public string Discovery { get; set; } = string.Empty;
            public string DestroyPiece { get; set; } = string.Empty;
            public string PutPiece { get; set; } = string.Empty;
            public string InformationExchange { get; set; } = string.Empty;
        }

        public class PositionDTO
        {

            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
