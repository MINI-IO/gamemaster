﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class WrongMoveDTO : ISendDTO
    {
        public PositionDTO Position { get; set; } = null!;

        public class PositionDTO
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
