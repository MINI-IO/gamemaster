﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class PenaltyErrorDTO : ISendDTO
    {
        public DateTime WaitUntil { get; set; }
    }
}
