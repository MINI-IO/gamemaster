﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class GameEndDTO : ISendDTO
    {
        public string Winner { get; set; } = string.Empty;
    }
}
