﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class WrongPutPieceDTO : ISendDTO
    {
        public string ErrorSubtype { get; set; } = string.Empty;
    }
}
