﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class PutPieceDTO : ISendDTO
    {
        public string PutPieceInfo { get; set; } = string.Empty;
    }
}
