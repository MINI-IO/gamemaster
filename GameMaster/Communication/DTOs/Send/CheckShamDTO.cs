﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    class CheckShamDTO : ISendDTO
    {
        public bool Sham { get; set; }
    }
}
