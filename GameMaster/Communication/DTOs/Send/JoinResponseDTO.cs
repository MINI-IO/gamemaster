﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class JoinResponseDTO : ISendDTO
    {
        public bool Accepted { get; set; }
        public int AgentId { get; set; }
    }
}
