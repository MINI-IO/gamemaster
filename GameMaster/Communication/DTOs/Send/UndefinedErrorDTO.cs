﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Send
{
    public class UndefinedErrorDTO : ISendDTO
    {
        public PositionDTO Position { get; set; } = null!;
        public bool HoldingPiece { get; set; }

        public class PositionDTO
        {
            public int x { get; set; }
            public int y { get; set; }
        }
    }
}
