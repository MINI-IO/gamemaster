﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Receive
{
    public class MoveDTO
    {
        public string Direction { get; set; } = string.Empty;
    }
}
