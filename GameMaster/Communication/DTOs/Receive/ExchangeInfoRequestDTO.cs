﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.DTOs.Receive
{
    public class ExchangeInfoRequestDTO
    {
        public int AskedAgentID { get; set; }
    }
}
