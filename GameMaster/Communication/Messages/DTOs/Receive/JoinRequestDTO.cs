﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Receive
{
    public class JoinRequestDTO
    {
        public string TeamId { get; set; } = string.Empty;
    }
}
