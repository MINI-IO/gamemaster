﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Receive
{
    public class ExchangeInfoResponseDTO
    {
        public int RespondToID { get; set; }
        public List<double> Distances { get; set; } = new List<double>();
        public List<string> RedTeamGoalAreaInformations { get; set; } = new List<string>();
        public List<string> BlueTeamGoalAreaInformations { get; set; } = new List<string>();
    }
}
