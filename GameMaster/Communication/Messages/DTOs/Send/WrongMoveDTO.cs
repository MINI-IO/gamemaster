﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    class WrongMoveDTO
    {
        public PositionDTO Position { get; set; } = null!;

        public class PositionDTO
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
