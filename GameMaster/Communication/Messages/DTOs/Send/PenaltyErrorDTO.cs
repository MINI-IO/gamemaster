﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class PenaltyErrorDTO
    {
        public DateTime WaitUntil { get; set; }
    }
}
