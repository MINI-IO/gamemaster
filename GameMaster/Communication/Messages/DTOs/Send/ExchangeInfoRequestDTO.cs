﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class ExchangeInfoRequestDTO
    {
        public int AskingID { get; set; }
        public bool Leader { get; set; }
        public string TeamId { get; set; } = string.Empty;
    }
}
