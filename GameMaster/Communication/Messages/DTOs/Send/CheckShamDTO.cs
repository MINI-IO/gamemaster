﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    class CheckShamDTO
    {
        public bool Sham { get; set; }
    }
}
