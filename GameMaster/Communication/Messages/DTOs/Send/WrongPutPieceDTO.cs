﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class WrongPutPieceDTO
    {
        public string ErrorSubtype { get; set; } = string.Empty;
    }
}
