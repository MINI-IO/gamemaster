﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class JoinResponseDTO
    {
        public bool Accepted { get; set; }
        public int AgentId { get; set; }
    }
}
