﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class GameEndDTO
    {
        public string Winner { get; set; } = string.Empty;
    }
}
