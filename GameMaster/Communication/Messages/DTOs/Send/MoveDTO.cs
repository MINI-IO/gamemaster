﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages.DTOs.Send
{
    public class MoveDTO
    {
        public bool MadeMove { get; set; }
        public CurrentpositionDTO CurrentPosition { get; set; } = null!;
        public int? ClosestPiece { get; set; }
        public bool PickedUpPiece { get; set; }

        public class CurrentpositionDTO
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
