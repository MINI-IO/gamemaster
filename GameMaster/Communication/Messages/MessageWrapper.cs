﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication.Messages
{
    public class MessageWrapper
    {
        public int MessageID { get; set; }
        public int AgentID { get; set; }
    }
}
