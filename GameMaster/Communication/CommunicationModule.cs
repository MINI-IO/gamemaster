﻿using GameMaster.Communication.Messages;
using IoCommunication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communication
{
    public static class CommunicationModule
    {
        public static IHostBuilder CreateTcpClient<T>(
            this IHostBuilder hostBuilder,
            BasicMessageHandler<T> basicMessageHandler) =>
                hostBuilder.ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<IMessageHandler>(basicMessageHandler);
                    services.AddHostedService<TcpClient>();
                });
    }
}