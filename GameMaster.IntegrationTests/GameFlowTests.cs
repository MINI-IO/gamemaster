using GameMaster.Model;
using GameMaster.Model.Fields;
using GameMaster.Model.Game;
using GameMaster.Model.Pieces;
using NSubstitute;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using Xunit;

namespace GameMaster.IntegrationTests
{
    public class GameFlowTests
    {
        private GameInstance Game { get; set; }

        private IScore RedTeamScore { get; set; }
        private IScore BlueTeamScore { get; set; }
        private Board Board { get; set; }
        private Agent BlueAgent => Game.Agents.First(a => a.Value.Team == Team.Blue).Value;
        private int BlueAgentId => 1;

        private int XMove => BlueAgent.Position.Id.X > 0
            ? BlueAgent.Position.Id.X - 1
            : BlueAgent.Position.Id.X + 1;
        private Direction MoveDir => BlueAgent.Position.Id.X > 0
            ? Direction.negativeX
            : Direction.positiveX;

        public GameFlowTests()
        {
            RedTeamScore = Substitute.For<IScore>();
            BlueTeamScore = Substitute.For<IScore>();
            Board = Substitute.ForPartsOf<Board>(
                new CheckersGoalPattern(10,10,3,3),
                BlueTeamScore,
                RedTeamScore,
                10,
                10,
                3,
                0,
                5);
            Board.ContainsField(default).ReturnsForAnyArgs(false);

            Game = new GameInstance(
                Substitute.For<IGameStopwatch>(),
                RedTeamScore,
                BlueTeamScore,
                Board,
                Substitute.For<ISenderService>(),
                new ConfigurationViewModel()
                {
                    MovePenaltyTime = 0,
                    InformationExchangePenaltyTime = 0,
                    DiscoverPenaltyTime = 0,
                    PutPenaltyTime = 0,
                    CheckShamPenaltyTime = 0,
                    DestroyPiecePenaltyTime = 0,
                    BoardHeight = 10,
                    BoardWidth = 10,
                    GoalAreaHeight = 2,
                    TeamPlayersNumber = 3,
                    NumberOfGoals = 3,
                    NumberOfPieces = 5,
                    ShamPieceProbability = 0.5,
                });

            Game.Join(BlueAgentId, Team.Blue);
        }

        [Fact]
        public void GameStartTest()
        {
            Assert.Single(Game.Agents.Where(a => a.Value == BlueAgent));
            Assert.Single(Board.Map.Where(p => p.Value.WhosHere == BlueAgent));
            Assert.Equal(
                BlueAgent.Position,
                Board.Map.Select(p => p.Value).First(p => p.WhosHere == BlueAgent));

            Game.Start();

            var expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);

            while (Board.GetFieldById(expectedPosition).ContainsPickUpablePiece)
                    Board.GetFieldById(expectedPosition).PickUpPiece();

            BlueAgent.Move(MoveDir);

            TestAllCases(0, 0, true, expectedPosition); // Moving without picking up piece

            expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);
            Board.GetFieldById(expectedPosition).PutPiece(new NormalPiece(Substitute.For<IPieceService>()));
            BlueAgent.Move(MoveDir);

            TestAllCases(0, 0, false, expectedPosition); // Moving with picking up piece

            expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);

            BlueAgent.Move(MoveDir);

            TestAllCases(0, 0, false, expectedPosition); // Moving with piece without scoring


            expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);

            var goalField = new GoalField(expectedPosition, BlueTeamScore, Substitute.For<IDistanceCalculator>());
            Board.GetFieldById(expectedPosition).Returns(goalField);
            BlueAgent.Move(MoveDir);
            BlueAgent.PutPiece();

            TestAllCases(1, 0, true, expectedPosition); // Moving to goal field with piece and score

            expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);

            var taskField = new TaskField(expectedPosition, Substitute.For<IDistanceCalculator>());
            taskField.PutPiece(new ShamPiece(Substitute.For<IPieceService>()));
            Board.GetFieldById(expectedPosition).Returns(taskField);
            BlueAgent.Move(MoveDir);

            TestAllCases(1, 0, false, expectedPosition); // Moving to task field again picks up piece

            expectedPosition = new Point(XMove, BlueAgent.Position.Id.Y);

            Board.GetFieldById(expectedPosition).Returns(goalField);
            BlueAgent.Move(MoveDir);
            BlueAgent.PutPiece();

            TestAllCases(1, 0, true, expectedPosition); // Moving to goal field with piece and puting doesnt increase points
        }

        private void TestAllCases(
            int bluePointsIncrease,
            int redPointsIncrease,
            bool nullPiece,
            Point expectedPosition)
        {

            BlueTeamScore.ReceivedWithAnyArgs(bluePointsIncrease).IncrementPoints();

            RedTeamScore.ReceivedWithAnyArgs(redPointsIncrease).IncrementPoints();

            if (nullPiece)
            {
                Assert.Null(BlueAgent.Holding);
            }
            else
            {
                Assert.NotNull(BlueAgent.Holding);
            }

            Assert.Equal(expectedPosition, BlueAgent.Position.Id);
        }
    }
}
