﻿using GameMaster.Model.Game;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace GameMaster.IntegrationTests
{
    public class GamePaceTests
    {
        private GameInstance Game { get; set; }
        private GameStopwatch Stopwatch { get; set; }
        private ISenderService Sender { get; set; }

        public GamePaceTests()
        {
            Stopwatch = Substitute.For<GameStopwatch>();
            Sender = Substitute.For<ISenderService>();
            Game = new GameInstance(
                Stopwatch,
                default,
                default,
                default,
                Sender,
                new ConfigurationViewModel()
                {
                    MovePenaltyTime = 0,
                    InformationExchangePenaltyTime = 0,
                    DiscoverPenaltyTime = 0,
                    PutPenaltyTime = 0,
                    CheckShamPenaltyTime = 0,
                    DestroyPiecePenaltyTime = 0,
                    BoardHeight = 10,
                    BoardWidth = 10,
                    GoalAreaHeight = 2,
                    TeamPlayersNumber = 3,
                    NumberOfGoals = 3,
                    NumberOfPieces = 5,
                    ShamPieceProbability = 0.5,
                });

            Game.Start();
        }

        [Fact]
        public void GameStart()
        {
            Sender.Received().SendStartMessage();
            Assert.False(Stopwatch.IsPaused());
        }

        [Fact]
        public void Pause()
        {
            Game.Pause();

            Sender.Received().SendPauseMessage();
            Assert.True(Stopwatch.IsPaused());
        }

        [Fact]
        public void Resume()
        {
            Game.Pause();
            Game.Resume();

            Sender.Received().SendResumeMessage();
            Assert.False(Stopwatch.IsPaused());
        }

        [Fact]
        public void End()
        {
            Game.End();

            Sender.Received().SendEndMessage();
            Assert.True(Stopwatch.IsPaused());
        }
    }
}
